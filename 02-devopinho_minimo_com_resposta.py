from slackclient import SlackClient
import time, os

BOT_ID = os.environ.get("BOT_ID")
slack_client = SlackClient(os.environ.get("SLACK_BOT_TOKEN"))
slack_client.rtm_connect()
print("Conectado à API do Slack com sucesso.")
while True:
    for message in slack_client.rtm_read():
        if message['type'] == 'message' and message['user'] != BOT_ID:
            text = message['text']
            channel = message['channel']
            print(text)
            response = input("Digite a resposta\n")
            slack_client.api_call(
                "chat.postMessage",
                channel=channel,
                text=response,
                as_user=True)
    time.sleep(2)
