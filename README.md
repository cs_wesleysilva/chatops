Devopynho, seu DevOps amiguinho
===============================

![Devopynho](./devopynho.png)

Olá, amiguinhos! Hoje vamos desvendar as magias negras ocultas das trevas escuras do mundo dos Chatbots do Slack :skull: :jack_o_lantern: :ghost:

Este repositório possui um código progressivo, que vai do lixo :toilet: ao luxo :gem: em 6 passsos, sempre incrementando o nível de dificuldade e sofisticação. Começa feio e simples e termina igual os programas da Concrete Solutions :heart_eyes:.

Mostramos nos exemplos como:

1.  Conectar na API do Slack :hash:
2.  Receber e enviar mensagens com Python :snake:
3.  Interagir com o SO e com o Jenkins :older_man:

Isso é um bot com inteligência artificial de verdade, amiguinhos? Claro que não :broken_heart: :scream:

Mas isso é um começo. Você precisa partir de algum ponto.

E tem outra coisa: um bot não precisa ser inteligente, ele precisa **parecer** inteligente. :bulb:

Você pode usar e abusar de recursos como expressões regulares para tentar capturar o maior número possível de comportamentos. Há muitas coisas prontas, mas você vai acabar esbarrando em um problema: **o idioma**. Quase tudo que existe pensa na estrutura gramatical do idioma inglês, o que torna as bibliotecas difíceis de reutilizar na terrinha do Temer.

Dá uma olhada, amiguinhos, nas formas de construir um bot lindo:

![bots lindos](./bots.png)

Algumas abordagens sugeridas para aprofundar os estudos nessa área:

-   Expressões Regulares
-   Redes Neurais
-   Long Short Term Memory Networks (LSTM)
-   Keras
-   Tensorflow

Até logo, amiguinhos :kissing_heart: !!!
