from slackclient import SlackClient
import time, random, os


def random_wisdom():
    quotes = [
        "Essa lei de andar com farou aceso de dia, será que a noite também precisa?",
        "Soldado, não te vi na aula de camuflagem hoje. - Obrigado capitão.",
        "Falam que o álcool causa a morte de muitos, mas não falam quantos nasceram por causa dele.",
        "Não é que eu tenha medo de morrer. Só não quero estar lá quando isso acontecer.",
        "É muito triste quando se é rico, bonito e sensual e aí vem o despertador e acaba com tudo.",
        "Se as pessoas dormem de conchinha, será que as conchas dormem de pessoinha?",
        "Sabe o que acontece quando você esfrega uma lâmpada embaixo d´água? Aparece um Hidrogênio.",
        "Os ursos polares adoram o frio. Os bipolares às vezes adoram, às vezes não...",
        "Antes de falar, ouça. Antes de agir, pense. Antes de desistir, tente. Antes de cagar, veja se tem papel.",
        "Se o Xororó fosse dono de uma sorveteria, nela teria uma sobremesa chamada Sundae Junior."
    ]
    return (random.choice(quotes))


BOT_ID = os.environ.get("BOT_ID")
slack_client = SlackClient(os.environ.get("SLACK_BOT_TOKEN"))
slack_client.rtm_connect()
print("Conectado à API do Slack com sucesso.")
while True:
    for message in slack_client.rtm_read():
        if message['type'] == 'message' and message['user'] != BOT_ID:
            text = message['text']
            channel = message['channel']
            print(text)
            response = random_wisdom()
            slack_client.api_call(
                "chat.postMessage",
                channel=channel,
                text=response,
                as_user=True)
    time.sleep(2)
