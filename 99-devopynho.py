from slackclient import SlackClient
import os
import time

# Pega o ID do Bot de uma variável de ambiente.
# Para obter o ID, execute o print_bot_id.py
BOT_ID = os.environ.get("BOT_ID")

# Me chamou?
AT_BOT = "<@" + BOT_ID + ">"

# Inicia o cliente Slack para Python usando o token via
# variável de ambiente
slack_client = SlackClient(os.environ.get("SLACK_BOT_TOKEN"))


def handle_command(command, channel):
    """ Esta é a função que recebe os comandos enviados pela API do Slack.
        Sempre que o bot receber uma mensagem, a mensagem e o canal serão
        passados como parâmetros para este método.
    """

    ### MAGICA AQUI

    response = ""
    print(response)
    slack_client.api_call(
        "chat.postMessage", channel=channel, text=response, as_user=True)


def parse_slack_output(slack_rtm_output):
    """ Esta função é chamada sempre que há uma nova mensagem. Se
        o texto da mensagem contém o nome do bot então o texto e o canal
        são retornados, senão None, None é retornado.
        O parâmetro slack_rtm_output é um JSON resornado pela
        API do Slack.
    """
    output_list = slack_rtm_output
    if output_list and len(output_list) > 0:
        for output in output_list:
            if output and 'text' in output and AT_BOT in output['text']:
                print("Mensagem recebida: %s, Canal: %s." %
                      (output['text'], output['channel']))
                return output['text'].split(AT_BOT)[1].strip().lower(), \
                       output['channel']
    return None, None


if __name__ == "__main__":
    # Vamos ler o Socket de 1 em 1 segundo
    READ_WEBSOCKET_DELAY = 1
    """ Este loop lê a cada segundo o buffer de mensagens do Slack.
        Caso tenha alguma mensagem, ele obtém e verifica se ela foi direcionada
        para o nosso bot. Se não foi, a mensagem é ignorada, se foi, ela é
        encaminhada para outro método, que trata a execução de uma ação.
    """
    if slack_client.rtm_connect():
        print("Conectado à API do Slack com sucesso.")
        while True:
            command, channel = parse_slack_output(slack_client.rtm_read())
            if command and channel:
                handle_command(command, channel)
            time.sleep(READ_WEBSOCKET_DELAY)
    else:
        print("Falha na conexão. Verifique o BOT ID ou o API Token.")
