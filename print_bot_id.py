"""
print_bot_id.py
---------------

Este script serve para exibir o SLACK TOKEN ID do seu bot.

Ajuste o valor da variável BOT_NAME para o mesmo nome que você
configurou no Slack quando criou a integração.

Você só precisa executá-lo uma vez e anotar o ID exibido.

"""

import os
from slackclient import SlackClient

BOT_NAME = ""

# Obtém o Token de conexão do Slack da variável de ambiente
slack_client = SlackClient(os.environ.get('SLACK_BOT_TOKEN'))

# Faz uma chamada na API do Slack e cria um objeto com todos os usuários
api_call = slack_client.api_call("users.list")

# Itera no objeto buscando nosso querido bot
if api_call.get('ok'):
    users = api_call.get('members')
    for user in users:
        if 'name' in user and user.get('name') == BOT_NAME:
            print("Bot ID for '" + user['name'] + "' is " + user.get('id'))
else:
    print("could not find bot user with the name " + BOT_NAME)
